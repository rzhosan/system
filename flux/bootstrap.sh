# export $GITLAB_TOKEN=

flux bootstrap gitlab \
  --owner=rzhosan \
  --repository=system \
  --branch=master \
  --path=flux/clusters/local \
  --token-auth \
  --personal \
  --components-extra=image-reflector-controller,image-automation-controller