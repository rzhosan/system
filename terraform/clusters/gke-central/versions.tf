terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.70.0"
    }
  }

  backend "gcs" {
    bucket  = "ml-rzhasan-tf-state"
    prefix  = "terraform/state"
  }

  required_version = "~> 0.15"
}
