output "zone" {
  value       = var.zone
  description = "GCloud Zone"
}

output "project_id" {
  value       = var.project_id
  description = "GCloud Project ID"
}

output "cluster_name" {
  value       = var.cluster_name
  description = "Kubernetes Cluster Name"
}
