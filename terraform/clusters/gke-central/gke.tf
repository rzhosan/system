resource "google_service_account" "default" {
  account_id   = "${var.cluster_name}-service-account"
  display_name = "${title(var.cluster_name)} Service Account"
}

resource "google_project_iam_binding" "gkehub" {
  role = "roles/gkehub.connect"
  members = [
    "serviceAccount:${google_service_account.default.email}"
  ]
}

resource "google_service_account_key" "key" {
  service_account_id = google_service_account.default.id
}

resource "google_container_cluster" "default" {
  name               = var.cluster_name
  location           = var.zone
  initial_node_count = 2
  network = google_compute_network.vpc.id
  subnetwork = google_compute_subnetwork.subnet.id

  node_config {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.default.email

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project_id
    }
  }

  timeouts {
    create = "30m"
    update = "40m"
  }
}
